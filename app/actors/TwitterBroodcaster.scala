package actors

import akka.actor.{ActorRef, Actor}
import akka.actor.Actor.Receive
import play.api.libs.ws.WS
import play.api.{Play, Logger}
import play.api.libs.iteratee.{Iteratee, Enumeratee, Concurrent, Enumerator}
import play.api.libs.json.{JsObject, Json}
import play.api.libs.oauth.{OAuthCalculator, RequestToken, ConsumerKey}
import play.extras.iteratees.{JsonIteratees, Encoding}
import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global

import scala._

class TwitterBroodcaster extends Actor {

  override def receive: Receive = {
    case value:String=>
      Logger.info("Subscribed to:" + self)
      subscribe(sender(), value)
  }

  private var broadcastEnumerator: Option[Enumerator[JsObject]] = None

  val credentials: Option[(ConsumerKey, RequestToken)] = for {
    apiKey <- Play.configuration.getString("twitter.apiKey")
    apiSecret <- Play.configuration.getString("twitter.apiSecret")
    token <- Play.configuration.getString("twitter.token")
    tokenSecret <- Play.configuration.getString("twitter.tokenSecret")
  } yield (
      ConsumerKey(apiKey, apiSecret),
      RequestToken(token, tokenSecret)
      )

  def connect(value: String): Unit = {
    credentials.map {
      case (consumerKey, requestToken) =>
        val (iteratee, enumerator) = Concurrent.joined[Array[Byte]]

        val jsonStream: Enumerator[JsObject] = enumerator &>
          Encoding.decode() &>
          Enumeratee.grouped(JsonIteratees.jsSimpleObject)

        val (be, _) = Concurrent.broadcast(jsonStream)

        broadcastEnumerator = Some(be)

        val url = "https://stream.twitter.com/1.1/statuses/filter.json"

        WS.url(url)
          .withRequestTimeout(-1)
          .sign(OAuthCalculator(consumerKey, requestToken))
          .withQueryString("track" -> value)
          .get { response =>
          Logger.info("Status: " + response.status)
          iteratee
        }.map { _ =>
          Logger.info("Twitter stream closed")
        }
    } getOrElse {
      Logger.error("Twitter credentials missing")
    }
  }

  def subscribe(out: ActorRef, value: String): Unit = {
    if (broadcastEnumerator == None) {
      connect(value)
    }
    val twitterClient = Iteratee.foreach[JsObject] { t => out ! t}

    broadcastEnumerator.map { enumerator =>
      enumerator.run(twitterClient)
    }
  }

}
