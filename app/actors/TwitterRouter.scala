package actors

import akka.actor._
import akka.routing.{RoundRobinPool, RoundRobinRouter}
import akka.util.Timeout
import models.UserRequest
import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.concurrent._
import play.api.Play.current

import scala.concurrent.duration.FiniteDuration


class TwitterRouter(out: ActorRef) extends Actor {

  import TwitterRouter._

  def receive = {
    case "hello" =>
      Logger.info("Received subscription from a client")
      out ! Json.obj("text" -> "Hello, world!")

    case "subscribe" =>
      Logger.info("Received subscription from a client")


    case jsonVal: String if jsonVal.startsWith("{") =>
      val userRequest = Json.parse(jsonVal).as[UserRequest]
      if (userRequest.action == "contains") {
        self forward userRequest
      }

    case UserRequest(action, value) =>
      //      val twitterBroadcaster = context.actorOf(Props[TwitterBroodcaster])
      router forward value
  }
}

object TwitterRouter {
  def props(out: ActorRef) = Props(new TwitterRouter(out))

  val router = Akka.system.actorOf(RoundRobinPool(5).props(Props[TwitterBroodcaster]), "router")
}
