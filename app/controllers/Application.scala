package controllers

import actors.{TwitterRouter}
import play.api.Play.current
import play.api.libs.json.JsValue
import play.api.mvc._

object Application extends Controller {

    def index = Action {
      Ok(views.html.main())
    }

  def tweets = WebSocket.acceptWithActor[String, JsValue] {
    request => out => TwitterRouter.props(out)
  }
}