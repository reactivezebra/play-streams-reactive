package models

import play.api.libs.json.{Json, Format}

case class UserRequest(action: String, value: String) {
}

object UserRequest {
  implicit val userRequestFormat: Format[UserRequest] = Json.format[UserRequest]
}