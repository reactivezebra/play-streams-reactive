var app = angular.module('TwitterStreams', []);
app.controller('MainCtrl', function ($scope) {
    console.log("MainCtrl started");
    $scope.size = 0;

    $scope.addPanel = function (keyword) {
        console.log("keyword = " + keyword);
        var id = getId();
        $('#main_row').append("<div class=\"col-md-4\"><div id=\"" + id + "\" class= \"twipanel\"></div></div>");
        connect(id, 1, keyword);
    };

    function getId() {
        $scope.size += 1;
        return $scope.size;
    }

    $scope.clear = function () {
        $(".twipanel").empty();
    }

});

var appendTweet = function (id, data) {
    $('#' + id).append("<p>" + data + "</p>");
};

var connect = function (id, attempt, content) {
    var connectionAttempt = attempt;
    var url = "ws://localhost:9000/tweets";
    var tweetSocket = new WebSocket(url);

    tweetSocket.onmessage = function (event) {
        //console.log(event);
        var data = JSON.parse(event.data);
        appendTweet(id, data.text);
    };

    tweetSocket.onopen = function () {
        connectionAttempt = 1;
        var message = {};
        message.action = "contains";
        message.value = content;
        tweetSocket.send(JSON.stringify(message));
    };

    tweetSocket.onclose = function () {
        if (connectionAttempt <= 3) {
            appendTweet(id, "WARNING: Lost server connection, attempting to reconnect. Attempt number " + connectionAttempt);
            setTimeout(function () {
                connect(id, connectionAttempt + 1, content);
            }, 5000);
        } else {
            alert("The connection with the server was lost.");
        }
    };
};